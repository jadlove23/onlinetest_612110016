﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class PlayerScore : MonoBehaviourPunCallbacks
{
   public static  int score;

  public void resetScore() 
   {
       photonView.RPC("ResetScore", RpcTarget.All);
   }


   [PunRPC]
   void RPC_AddScore(int amount)
   {
       
      score += amount;
      if (score > 9)
      {
          StartCoroutine(GameManager.instance.RestartGame());
      }
      GameUI.SetScore(score); 
   }

  
}
