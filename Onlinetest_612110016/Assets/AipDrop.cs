﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AipDrop : MonoBehaviour
{
   public static Vector3 RadomPos(float yOffset)
   {
      var  spwnPos=new  Vector3(Random.Range(-30.0f,30.0f),yOffset,Random.Range(-30.0f,30.0f));
      return spwnPos;
   }

   public static Quaternion RandomRotation()
   {
      var spawnRot = Quaternion.Euler(1, Random.Range(0, 180), 0);
      return spawnRot;
   }
}
