﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HUD : MonoBehaviour
{
   private static HUD instance;

   private void Awake()
   {
       if (instance != null)
       {
           DestroyImmediate(gameObject);
           return;
           
       }

       instance = this;
       DontDestroyOnLoad(gameObject);
   }
}
