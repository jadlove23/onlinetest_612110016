﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;


public class Missile : MonoBehaviour
    {
        [SerializeField] ParticleSystem trail;
        [SerializeField] ParticleSystem explosion;
        [SerializeField] Rigidbody rbody;
        public int damage = 10;
        public PlayerShoot owner { get; private set; }
        float speed = 60f;

        void FixedUpdate()
        {
            rbody.MovePosition(transform.position + transform.forward * speed * Time.fixedDeltaTime);
        }

        public void SetOwner(PhotonView ownerView)
        {
            owner = ownerView.GetComponent<PlayerShoot>();
        }

        /*void OnTriggerEnter(Collider other)
        {
            if(other.CompareTag("Player"))
            {
                var hitPlayer = other.GetComponent<PlayerHealth>();

                if (hitPlayer.photonView.IsMine)
                {
                    if (owner.photonView.ViewID == hitPlayer.photonView.ViewID) return;
                    hitPlayer.DoDamage(this);
                }
                DestroyMissile();
            }

            DestroyMissile();
        }
            */
        private void OnCollisionEnter(Collision other)
        {
            if(other.collider.CompareTag("Player"))
            {
                Debug.Log("hit");
                var hitPlayer = other.collider.GetComponent<PlayerHealth>();

                if (hitPlayer.photonView.IsMine)
                {
                    if (owner.photonView.ViewID == hitPlayer.photonView.ViewID) return;
                    hitPlayer.DoDamage(this);
                }
                DestroyMissile();
            }

            DestroyMissile();
        }


        void DestroyMissile()
        {
            trail.transform.SetParent(null);
            trail.Stop(true, ParticleSystemStopBehavior.StopEmitting);
            explosion.transform.SetParent(null);
            explosion.Play();
            Destroy(gameObject);
        }
    } 
