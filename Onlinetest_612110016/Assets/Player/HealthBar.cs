﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;

namespace PUNTutorial
{
    public class HealthBar : MonoBehaviourPunCallbacks
    {
        [SerializeField] Image healthBarImage;

        public void SetHealthBarValue(float value)
        {
            healthBarImage.fillAmount = value;
            healthBarImage.color = Color.Lerp(Color.red, Color.green, value);
            if (photonView!=null)
            {
                photonView.RPC("RPC_HealthBar",RpcTarget.All,value);
            }
        }

        [PunRPC]
        void RPC_HealthBar(float value)
        {
            healthBarImage.fillAmount = value;
        }
    }
}
