﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class PlayerShoot : MonoBehaviourPunCallbacks
{
   [SerializeField] private Missile missilePrefab;
    private float nextFireTime;
    private float fireDelay = 0.25f;

    private void Awake()
    {
        enabled = photonView.IsMine;
    }

    private void Update()
    {
        if (Time.time < nextFireTime) return;

        if (Input.GetAxis("Fire1") > 0)
        {
            nextFireTime = Time.time + fireDelay;
            photonView.RPC("RPC_FireMissile",RpcTarget.All);
        }
    }

    [PunRPC]
    void RPC_FireMissile(PhotonMessageInfo info)
    {
        var missile = Instantiate(missilePrefab, transform.position, transform.rotation);
        missile.SetOwner(info.photonView);
        missile.gameObject.SetActive(true);
    }
}
