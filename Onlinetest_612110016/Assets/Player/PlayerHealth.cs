﻿using System;
using System.Collections;
using System.Collections.Generic;
using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using PUNTutorial;

using UnityEngine;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public class PlayerHealth : MonoBehaviourPunCallbacks,IOnEventCallback
{
   [SerializeField] HealthBar healthBar;
   [SerializeField] TankExploder tankExploderPrefab;
   private const int MAX_HP = 100;
   private Player _player;
   public Player player
   {
      get
      {
         if (_player == null) _player = GetComponent<Player>();
         return _player;
      } 
   }

  

   int hitPoints
   {
      get
      {
         object hp;
         if (photonView.Owner.CustomProperties.TryGetValue("HP", out hp))
            return (int)hp;
         else
         {
            return MAX_HP;
         }
      }
      set
      {
         player.SetCustomProperty("HP",value);
      }
   }

   public void DoDamage(Missile missile)
   {
     hitPoints  = Mathf.Clamp(hitPoints - missile.damage, 0, MAX_HP);
     DisplayHealth(); 
     if (hitPoints <= 1)
     {
        missile.owner.photonView.RPC("RPC_AddScore", missile.owner.photonView.Owner, 1);
        StartCoroutine(ExplodeTank());
     }
   }
   public void DoDamageObtacle(int amount,int ownerID)
   {
      hitPoints  = Mathf.Clamp(hitPoints - amount, 0, MAX_HP);
      DisplayHealth();
      if (hitPoints == 0)
      {
         StartCoroutine(ExplodeTank());
      }
   }



   string GetHealthString()
   {
      return new System.Text.StringBuilder()
         .Append("Health = ")
         .Append(hitPoints / (float)MAX_HP * 100)
         .Append("%")
         .ToString();
   }
  /* public  void OnPhotonPlayerPropertiesChanged(object[] playerAndUpdatedProps)
   {
      var plyr =(Photon.Realtime.Player)playerAndUpdatedProps[0];
      if (plyr.ActorNumber == photonView.OwnerActorNr)
      {
          var props = (ExitGames.Client.Photon.Hashtable)playerAndUpdatedProps[1];
         if (props.ContainsKey("HP"))
            DisplayHealth();
      }
   }*/

   public override void OnPlayerPropertiesUpdate(Photon.Realtime.Player targetPlayer, Hashtable changedProps)
   {
      base.OnPlayerPropertiesUpdate(targetPlayer, changedProps);
      var plyr = targetPlayer;
      if (plyr.ActorNumber == photonView.OwnerActorNr)
      {
         var props = (ExitGames.Client.Photon.Hashtable)changedProps;
         if (props.ContainsKey("HP"))
            DisplayHealth();
      }
   }

   void DisplayHealth()
   {
      healthBar.SetHealthBarValue(GetNormalisedHealthPercent(hitPoints));
      if (photonView.IsMine)
      {
         GameUI.SetHealth(GetNormalisedHealthPercent(hitPoints));
      }
   }
   float GetNormalisedHealthPercent(int hp)
   {
      return hp / (float)MAX_HP;
   }
   IEnumerator ExplodeTank()
   {
      StartCoroutine(GameManager.instance.DisplayMessage("DEAD")); 
      photonView.RPC("RPC_ExplodeTank", RpcTarget.All);
      yield return new WaitForSeconds(7f);
      photonView.RPC("RPC_RespawnTank", RpcTarget.All);
   }

   IEnumerator gameOver()
   {
      photonView.RPC("RPC_ExplodeTank", RpcTarget.All);
      yield return new WaitForSeconds(5f);
      GameUI.SetScore(0);
      PhotonNetwork.LeaveRoom();
   }

   public void OnEvent(EventData photonEvent)
   {
      if (photonEvent.Code == GameManager.ReGameCode)
      {
         
         StartCoroutine(GameManager.instance.DisplayMessage("Game OVER"));
         StartCoroutine(gameOver());
         //Time.timeScale=0;
      }
      
   }

   private void OnEnable()
   {
     PhotonNetwork.AddCallbackTarget(this); 
   }

   private void OnDisable()
   {
      PhotonNetwork.RemoveCallbackTarget(this);
   }

   [PunRPC]
   void RPC_ExplodeTank()
   {
      player.playerMovement.enabled = false;
      player.playerShoot.enabled = false;
      player.tankCollider.enabled = false;
      transform.GetChild(0).gameObject.SetActive(false);
      Instantiate(tankExploderPrefab, transform.position, transform.rotation);
   }

   [PunRPC]
   void RPC_RespawnTank()
   {
      var spawnPoint = GameManager.instance.GetRandomSpawnPoint();
      if(photonView.IsMine)
      {
         player.playerMovement.enabled = true;
         player.playerShoot.enabled = true;
         player.tankCollider.enabled = true;
      }
      hitPoints = MAX_HP;
      transform.GetChild(0).gameObject.SetActive(true);
      transform.position =spawnPoint.position;
   }
  
}
