﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class Player : MonoBehaviourPunCallbacks,IPunInstantiateMagicCallback
{
   public GameObject playerCam;
   public TankMoveMent playerMovement { get; private set; }
   public PlayerShoot playerShoot { get; private set; }
   public PlayerHealth playerHealth { get; private set; }
   public Collider tankCollider { get; private set; }
   private void Awake()
   {
     DontDestroyOnLoad(gameObject);
     
     playerMovement = GetComponent<TankMoveMent>();
     playerShoot = GetComponent<PlayerShoot>();
     playerHealth = GetComponent<PlayerHealth>();
     tankCollider = GetComponent<Collider>();
     
     if (!photonView.IsMine)
     {
         playerCam.GetComponent<Camera>().enabled=false;
         playerMovement.enabled = false;
         playerShoot.enabled = false;
         playerHealth.enabled = false;
         tankCollider.enabled = false;
     }
   }
   
   public void OnPhotonInstantiate(PhotonMessageInfo info)
   {
       name = new System.Text.StringBuilder()
           .Append(photonView.Owner.NickName)
           .Append(" [")
           .Append(photonView.ViewID)
           .Append("]")
           .ToString();

       BroadcastMessage("OnInstantiate", info, SendMessageOptions.DontRequireReceiver);
   }

   public void SetCustomProperty(string propName, object value)
   {
       ExitGames.Client.Photon.Hashtable prop = new ExitGames.Client.Photon.Hashtable();
       prop.Add(propName, value);
       photonView.Owner.SetCustomProperties(prop);
   }
 
}
