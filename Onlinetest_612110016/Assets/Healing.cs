﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class Healing : MonoBehaviourPunCallbacks
{ public int damage = 10;
    // private void OnCollisionEnter(Collision other)
    // {
    //     if(other.gameObject.CompareTag("Healing"))
    //     {
    //         Debug.Log("hitHealing");
    //         PlayerHealth hitPlayer = other.gameObject.GetComponent<PlayerHealth>();
    //         Debug.Log(hitPlayer.photonView.ViewID);
    //         hitPlayer.DoDamageObtacle(damage,hitPlayer.photonView.ViewID);
    //         photonView.RPC("PunHealing",RpcTarget.MasterClient); 
    //     }
    //    
    // }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            Debug.Log("hitHealing");
            PlayerHealth hitPlayer = other.gameObject.GetComponent<PlayerHealth>();
            Debug.Log(hitPlayer.photonView.ViewID);
            hitPlayer.DoDamageObtacle(damage,hitPlayer.photonView.ViewID);
            photonView.RPC("PunHealing",RpcTarget.MasterClient); 
        } 
    }

    [PunRPC]
    private void PunHealing()
    {
        Destroy(this.gameObject);
    }

    private void OnDestroy()
    {
       if(!photonView.IsMine)
           return;
       
       PhotonNetwork.Destroy(this.gameObject);
    }
}
