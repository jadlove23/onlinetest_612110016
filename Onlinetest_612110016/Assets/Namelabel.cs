﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using  TMPro;
[RequireComponent(typeof(TextMeshProUGUI))]
public class Namelabel : MonoBehaviour
{
    void onInstantiate(PhotonMessageInfo info)
    {
        var pView = GetComponentInParent<PhotonView>();
        if (pView.IsMine)
        {
            gameObject.SetActive(false);
            return;
            ;
        }

        GetComponent<TextMeshProUGUI>().text = pView.Owner.NickName;
    } 
}
