﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Pun.Simple;
using Photon.Realtime;
using UnityEngine;
using Random = UnityEngine.Random;
using  UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviourPunCallbacks
{
    public static GameManager instance;
    public static GameObject localPlayer;
     GameObject defaultSpawnPoint;
     public Text messageTxt;
     private PlayerScore _score;
     public static readonly byte ReGameCode = 1;

     private bool isGamestart = false;
     private bool isFirstSetting = false;
     public GameObject Healing;
     public int numOfHealing = 5;
     private float m_count = 0;
     public float m_countDropHeal = 10;
     void Awake()
     {
         if (instance != null)
         {
             DestroyImmediate(gameObject);
             return;
         }

         DontDestroyOnLoad(gameObject);
         instance = this;

         defaultSpawnPoint = new GameObject("Default SpawnPoint");
         defaultSpawnPoint.transform.position = new Vector3(0, 0.5f, 0);
         defaultSpawnPoint.transform.SetParent(transform, false);
         PhotonNetwork.AutomaticallySyncScene = true;
     }

     void Start()
     {
        
         PhotonNetwork.ConnectUsingSettings();
         
     }

     private void AirDripHealing()
     {
         if (isFirstSetting == false)
         {
             isFirstSetting = true;
             int half = numOfHealing / 2;
             for (int i = 0; i < half; i++)
             {
                 PhotonNetwork.InstantiateSceneObject(Healing.name,
                     AipDrop.RadomPos(5f),
                     AipDrop.RandomRotation(), 0);
                 
                // Debug.Log("start drop");
             }
     
             m_count = m_countDropHeal;
         }
         else
         {
             if (GameObject.FindGameObjectsWithTag("Healing").Length < numOfHealing)
             {
                 m_count -= Time.deltaTime;
                 if (m_count <= 0)
                 {
                     m_count = m_countDropHeal;
                     PhotonNetwork.Instantiate(Healing.name,
                         AipDrop.RadomPos(10f),
                         AipDrop.RandomRotation(), 0);
                     //Debug.Log("start drop");
                 }
             }
             {
                 
             }
         }
     }

     private void Update()
     {
        if(PhotonNetwork.IsMasterClient!=true)
            return;
     
        if (isGamestart == true)
        {
           
            AirDripHealing();
        }
     }

     public IEnumerator  RestartGame()
     { yield return  new  WaitForSeconds(2);
       RaiseEventOptions raiseEventOptions= new RaiseEventOptions{Receivers = ReceiverGroup.All};
       ExitGames.Client.Photon.SendOptions sendOptions=new ExitGames.Client.Photon.SendOptions{Reliability = true};
       PhotonNetwork.RaiseEvent(ReGameCode, null, raiseEventOptions, sendOptions);
     }
     
     public void JoinGame()
    {
        RoomOptions ro = new RoomOptions();
        ro.MaxPlayers = 6;
        PhotonNetwork.JoinOrCreateRoom("Default Room", ro, null);
    }
    
    public override void OnCreatedRoom()
    {
        Debug.Log("Create room succ");
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        Debug.Log("Create room failed");
    }
    public override void OnJoinedRoom ()
    {
        Debug.Log("Joined Room");
        if (PhotonNetwork.IsMasterClient)
        {
            PhotonNetwork.LoadLevel("GAMESCENE");
            isGamestart = true;
        }
    }

    public override void OnLeftRoom()
    {
        PhotonNetwork.LoadLevel("JoinGAME SCENE");
        PlayerScore.score = 0;
    }

    void OnLevelWasLoaded(int levelNumber)
    {
        if (!PhotonNetwork.InRoom) return;

        var spawnPoint = GetRandomSpawnPoint(); 

        localPlayer = PhotonNetwork.Instantiate(
            "Player",
            spawnPoint.position,
            spawnPoint.rotation, 0);
        StartCoroutine(DisplayMessage("Fight!"));
       
    }
    public Transform GetRandomSpawnPoint()
    {
        var spawnPoints = GetAllGameObjectsOfTypeInScene<SpawnPoint>(); 
        if (spawnPoints.Count == 0)
        {
            return defaultSpawnPoint.transform;
        }
        else
        {
            return spawnPoints[Random.Range(0, spawnPoints.Count)].transform;
        }
    }
    
    public static List<GameObject> GetAllGameObjectsOfTypeInScene<T>()
    {
        List<GameObject> objectsInScene=new List<GameObject>();
        foreach (GameObject go in Resources.FindObjectsOfTypeAll(typeof(GameObject)) as GameObject[])
        {
            if (go.hideFlags == HideFlags.NotEditable ||
                go.hideFlags == HideFlags.HideAndDontSave)
                continue;
            
            if(go.GetComponent<T>()!=null)
                objectsInScene.Add(go);
        }

        return objectsInScene;
    }

   public IEnumerator DisplayMessage(string message)
    {
        messageTxt.text = message;
        yield return new WaitForSeconds(2);
        messageTxt.text = "";
    }
}
